VENV := .venv
PYTHON := $(VENV)/bin/python3
PIP_COMPILE := $(VENV)/bin/pip-compile
PIP_SYNC := $(VENV)/bin/pip-sync
PRE_COMMIT := $(VENV)/bin/pre-commit
NOX := $(VENV)/bin/nox

.PHONY: all
all: $(VENV)

$(VENV):
	$(notdir $(PYTHON)) -m venv "$@"

$(PYTHON): | $(VENV)

.PHONY: build
build: | $(PYTHON)
	$| -m pip install --upgrade build
	$| -m build

.PHONY: install editable
editable: editable := --editable
install editable: | $(PYTHON)
	$| -m pip install $(editable) .

$(PIP_COMPILE) $(PIP_SYNC) &: | $(PYTHON)
	$| -m pip install pip-tools

.PHONY: pin sync
pin sync: requirements.txt
.PHONY: pin-dev sync-dev
pin-dev sync-dev: requirements-dev.txt
.PHONY: pin-test sync-test
pin-test sync-doc: requirements-test.txt
.PHONY: pin-doc sync-doc
pin-doc sync-doc: requirements-doc.txt

requirements.txt: pyproject.toml | $(PIP_COMPILE)
	$| \
        --allow-unsafe \
        --resolver="backtracking" \
        --generate-hashes \
        --output-file="$@" \
        "$<"

requirements-%.txt: pyproject.toml | $(PIP_COMPILE)
	$| \
        --allow-unsafe \
        --resolver="backtracking" \
        --extra="$*" \
        --output-file="$@" \
        "$<"

.PHONY: pin-upgrade
pin-upgrade: upgrade-requirements.txt
.PHONY: pin-dev-upgrade
pin-dev-upgrade: upgrade-requirements-dev.txt
.PHONY: pin-test-upgrade
pin-test-upgrade: upgrade-requirements-test.txt
.PHONY: pin-doc-upgrade
pin-doc-upgrade: upgrade-requirements-doc.txt

.PHONY:upgrade-requirements.txt
upgrade-requirements.txt: upgrade-%: pyproject.toml | $(PIP_COMPILE)
	$| \
        --upgrade \
        --generate-hashes \
        --allow-unsafe \
        --resolver="backtracking" \
        --output-file="$*" \
        "$<"

.PHONY: upgrade-requirements-%.txt
upgrade-requirements-%.txt: pyproject.toml | $(PIP_COMPILE)
	$| \
        --upgrade \
        --extra="$*" \
        --generate-hashes \
        --allow-unsafe \
        --resolver="backtracking" \
        --output-file="$(patsubst upgrade-%,%,$@)" \
        "$<"

.PHONY: sync
sync: requirements.txt
.PHONY: sync-dev
sync-dev: requirements-dev.txt
.PHONY: sync-test
sync-test: requirements-test.txt
.PHONY: sync-doc
sync-doc: requirements-doc.txt

sync sync-dev sync-test sync-doc: | $(PIP_SYNC)
	$| $<

.PHONY: pre-commit-install pre-commit-uninstall
pre-commit-install pre-commit-uninstall: pre-commit-%: | $(PRE_COMMIT)
	$| $*

.PHONY: lint
lint: nox-lint

.PHONY: test
test: unittest functest

.PHONY: unittest
unittest: nox-unittest

.PHONY: functest
functest: nox-functest

.PHONY: nox-lint nox-unittest nox-functest
nox-lint nox-unittest nox-functest: nox-%: | $(NOX)
	$| --reuse-existing-virtualenvs --no-install --session="$*"

debian/changelog:
	dch \
        --create \
        --package="python3-tomible" \
        --urgency=low \
        --newversion=0.0.1 \
        "Initial release"

.PHONY: pycache-remove
pycache-remove:
	find . | grep -E "(__pycache__|\.pyc|\.pyo$$)" | xargs rm -rf

.PHONY: mypycache-remove
mypycache-remove:
	find . | grep -E ".mypy_cache" | xargs rm -rf

.PHONY: pytestcache-remove
pytestcache-remove:
	find . | grep -E ".pytest_cache" | xargs rm -rf

.PHONY: build-remove
build-remove:
	rm -rf build/

.PHONY: clean
cleanup: pycache-remove mypycache-remove pytestcache-remove
