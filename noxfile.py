"""Nox configuration"""


import nox


@nox.session(venv_backend="venv")
def lint(session: nox.Session) -> None:
    """Run the static lint tests"""

    session.install("--requirement", "requirements-test.txt")
    session.install("-e", ".", "--no-deps")

    session.run("isort")
    session.run("flake8")
    session.run("black", ".")
    session.run("mypy", ".")
    session.run("pylint", "src", "tests", "noxfile.py")
    session.run(
        "bandit",
        "--configfile=pyproject.toml",
        "--recursive",
        "src",
        "tests",
        "noxfile.py",
    )
    session.run("safety", "check", "--output=bare")


@nox.session(venv_backend="venv")
def unittest(session: nox.Session) -> None:
    """Run the unit tests"""

    session.install("--requirement", "requirements-test.txt")
    session.install("-e", ".", "--no-deps")

    session.run("coverage", "run", "-m", "pytest", "-m", "not functional")
    session.run("coverage-badge", "-f", "-o", "assets/images/coverage.svg")


@nox.session(venv_backend="venv")
def functest(session: nox.Session) -> None:
    """Run the unit tests"""

    session.install("--requirement", "requirements-test.txt")
    session.install("-e", ".", "--no-deps")

    session.run("pytest", "-m", "functional")
