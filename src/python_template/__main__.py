"""CLI entrypoint"""


def main() -> None:
    """Base CLI entrypoint"""

    print("hello")


if __name__ == "__main__":
    main()
